﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov
{
    class MouseStatemeent
    {
        public event EventHandler Click;
        public event EventHandler Hover;
        public MouseState CurrentMouse { get; set; }
        public MouseState PreviousMouse { get; set; }
        public bool IsHovering { get; set; }
        private static MouseStatemeent instance = null;
        

        public static MouseStatemeent Instance
        {
            get
            {
                if (instance != null)
                {
                    instance = new MouseStatemeent();
                }
                return instance;
            }
        }

        public MouseStatemeent()
        {
            IsHovering = true;
        }

        public void MakeOverOrClick(Rectangle rectangle)
        {
            PreviousMouse = CurrentMouse;
            CurrentMouse = Mouse.GetState();

            var mouseRectangle = new Rectangle(CurrentMouse.X, CurrentMouse.Y, 1, 1);

            // if mouse intersect with rectangle
            if (mouseRectangle.Intersects(rectangle))
            {
                // call hover event if IsHovering is true
                if (IsHovering)
                {
                    Hover?.Invoke(this, new EventArgs());
                    IsHovering = false;
                }

                if (CurrentMouse.LeftButton == ButtonState.Released && PreviousMouse.LeftButton == ButtonState.Pressed)
                {
                    if (Click != null)
                    {
                        Click(this, new EventArgs());
                    }
                }
            }
            else
            {
                IsHovering = true;
            }
        }
    }
}
