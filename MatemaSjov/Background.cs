﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MatemaSjov
{
    class Background : GameObject, IDrawable
    {
        public int DrawOrder { get; set; }
        public Background(Texture2D texture, SpriteFont font) : base(texture, font)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var colour = Color.White;
            if (MouseStatemeent.Instance.IsHovering)
                colour = Color.Gray;

            spriteBatch.Draw(Texture, Rectangle, colour);
        }
    }
}
