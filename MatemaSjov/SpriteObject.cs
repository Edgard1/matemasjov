﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MatemaSjov
{
    class SpriteObject : GameObject, IDrawable
    {
        
        public int DrawOrder { get; set; }
        public SpriteObject(Texture2D texture, SpriteFont font) : base(texture, font)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var colour = Color.White;
            if (MouseStatemeent.Instance.IsHovering)
                colour = Color.Gray;

            spriteBatch.Draw(Texture, Rectangle, colour);
        }

    }
}