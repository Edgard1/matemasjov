﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov
{
    abstract class GameObject
    {
        #region Fields     
        public SpriteFont Font { get; set; }  
        public Texture2D Texture { get; set; }
        #endregion

        #region Properties
        public bool Clicked { get;  set; }
        public int ClickedCount { get;  set; }
        public bool FigurClicked { get;  set; }
        public Color PenColour { get; set; }
        public Vector2 Position { get; set; }
        public double Scale { get; set; }
        public Rectangle Rectangle
        {
            get
            {
                //return new Rectangle((int)Position.X, (int)Position.Y, Texture.Width , Texture.Height);
                return new Rectangle((int)Position.X, (int)Position.Y, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));
                
            }
        }
        public string Text { get; set; }
        #endregion

        public GameObject(Texture2D texture, SpriteFont font)
        {
            this.Texture = texture;
            this.Font = font;
            this.Scale = Scale;
        }

    }
}
