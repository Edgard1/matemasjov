﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov.Data
{
    class Analytic
    {
        private DbAccess dbAccess = new DbAccess();
        private SqlCommand CMD = new SqlCommand();

        public void AddAnalytic(string gameName, int wrong, int right)
        {
            CMD = new SqlCommand("INSERT INTO MatemasjovTblAnalytic (GameName, False, True, Dato) VALUES(@GameName, @False, @True, @Dato)");
            CMD.Parameters.AddWithValue("@GameName", gameName);
            CMD.Parameters.AddWithValue("@False", wrong);
            CMD.Parameters.AddWithValue("@True", right);
            CMD.Parameters.AddWithValue("@Dato", DateTime.Now);

            dbAccess.ModifyDataGetScope(CMD);
        }

        public DataTable GetLastIdFromAnalytic()
        {
            CMD = new SqlCommand("SELECT * FROM MatemasjovTblAnalytic WHERE Id = @Id");
            CMD.Parameters.AddWithValue("@Id", DbAccess.scope);
            return dbAccess.GetData(CMD);
        }

      
    }
}
