﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace MatemaSjov.Data
{
    class DbAccess
    {
        public static int scope;
        //readonly string _strDb = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
        readonly string _strDb = @"";

        public DataTable GetData(SqlCommand cmd)
        {
            DataSet objDs = new DataSet();

            SqlConnection objConn = new SqlConnection(_strDb);
            SqlDataAdapter objDa = new SqlDataAdapter();

            cmd.Connection = objConn;
            objDa.SelectCommand = cmd;

            objDa.Fill(objDs);
            objConn.Close();

            return objDs.Tables[0];

        }

        public void ModifyData(SqlCommand cmd)
        {
            //Start modify 
            SqlConnection objConn = new SqlConnection(_strDb);
            cmd.Connection = objConn;
            objConn.Open();
            cmd.ExecuteNonQuery();
            objConn.Close();
        }

        public int ModifyDataGetScope(SqlCommand cmd)
        {
            cmd.CommandText = cmd.CommandText + " SELECT SCOPE_IDENTITY()";

            SqlConnection objConn = new SqlConnection(_strDb);
            cmd.Connection = objConn;
            objConn.Open();

            scope = Convert.ToInt32(cmd.ExecuteScalar());
            Console.WriteLine(scope);
            objConn.Close();

            return scope;

        }
    }
}
