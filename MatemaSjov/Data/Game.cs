﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov.Data
{
    class Game
    {

       private DbAccess dbAccess = new DbAccess();
       private SqlCommand cmd = new SqlCommand();

        public DataTable GetGame(string gameName)
        {
            cmd = new SqlCommand("SELECT * FROM MatemasjovTblGame WHERE GameName = @GameName");
            cmd.Parameters.AddWithValue("@GameName", gameName);
            return dbAccess.GetData(cmd);
        }
    }
}
