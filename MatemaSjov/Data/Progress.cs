﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov.Data
{
    class Progress
    {
        private DbAccess dbAccess = new DbAccess();
        private SqlCommand CMD = new SqlCommand();

        public int Progresse { get; set; }
        public int Part { get; set; }

        public void AddProgress(int userId, string gameName, int progress, int partNr)
        {
            CMD = new SqlCommand("INSERT INTO MatemasjovTblProgress (UserId, GameName, Progress, Dato, PartNr) VALUES(@UserId, @GameName, @Progress, @Dato, @PartNr)");
            CMD.Parameters.AddWithValue("@UserId", userId);
            CMD.Parameters.AddWithValue("@GameName", gameName);
            CMD.Parameters.AddWithValue("@Progress", progress);
            CMD.Parameters.AddWithValue("@Dato", DateTime.Now);
            CMD.Parameters.AddWithValue("@PartNr", partNr);
            dbAccess.ModifyData(CMD);
        }

        public void UpdateProgressFromId(int userId, int partNr)
        {
            CMD = new SqlCommand("UPDATE MatemasjovTblProgress SET Progress = @Progress where UserId = @UserId AND Progress = @partNr");
            CMD.Parameters.AddWithValue("@UserId", userId);
            CMD.Parameters.AddWithValue("@Progress", Progresse);
            CMD.Parameters.AddWithValue("@partNr", partNr);
            dbAccess.ModifyData(CMD);
        }

        public DataTable GetAllInProgressFrom100Progress(int userId, int progress)
        {
            CMD = new SqlCommand("SELECT * FROM MatemasjovTblProgress WHERE Progress = @Progress AND UserId = @UserId");
            CMD.Parameters.AddWithValue("@Progress", 100);
            CMD.Parameters.AddWithValue("@UserId", userId);
            return dbAccess.GetData(CMD);
        }

        public DataTable GetUserIAndGameName(string gameName, int userId)
        {
            CMD = new SqlCommand("SELECT * FROM MatemasjovTblProgress WHERE GameName = @GameName AND UserId = @UserId");
            CMD.Parameters.AddWithValue("@GameName", gameName);
            CMD.Parameters.AddWithValue("@UserId", userId);
            return dbAccess.GetData(CMD);
        }
    }
}
