﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov.Data
{
    class Play
    {
        private DbAccess dbAccess = new DbAccess();
        private SqlCommand CMD = new SqlCommand();

        public DataTable GetUserIAndGameName(string gameName, int userId)
        {
            CMD = new SqlCommand("SELECT * FROM MatemasjovTblPlay WHERE GameName = @GameName AND UserId = @UserId");
            CMD.Parameters.AddWithValue("@GameName", gameName);
            CMD.Parameters.AddWithValue("@UserId", userId);
            return dbAccess.GetData(CMD);
        }

        public void AddPlay(string gameName, int userId)
        {
            CMD = new SqlCommand("INSERT INTO MatemasjovTblPlay (GameName, Dato,  UserId) VALUES(@GameName,  @Dato, @UserId)");
            CMD.Parameters.AddWithValue("@GameName", gameName);
            CMD.Parameters.AddWithValue("@Dato", DateTime.Now);
            CMD.Parameters.AddWithValue("@UserId", userId);
            dbAccess.ModifyData(CMD);

        }
    }
}
