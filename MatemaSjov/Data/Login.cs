﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov.Data
{
    class Login
    {
        DbAccess dbAccess = new DbAccess();
        SqlCommand cmd = new SqlCommand();

        public DataTable Getlogin(string color1, string color2, string color3)
        {
            cmd = new SqlCommand("SELECT * FROM MatemasjovTblUser WHERE Color1 = @Color1 AND Color2 = @Color2 AND Color3 = @Color3");
            cmd.Parameters.AddWithValue("@Color1", color1);
            cmd.Parameters.AddWithValue("@Color2", color2);
            cmd.Parameters.AddWithValue("@Color3", color3);
            return dbAccess.GetData(cmd);
        }

        public DataTable GetAllInlogin(string color1, string color2, string color3)
        {
            cmd = new SqlCommand("SELECT * FROM MatemasjovTblUser WHERE Color1 = @Color1 AND Color2 = @Color2 AND Color3 = @Color3");
            cmd.Parameters.AddWithValue("@Color1", color1);
            cmd.Parameters.AddWithValue("@Color2", color2);
            cmd.Parameters.AddWithValue("@Color3", color3);
            return dbAccess.GetData(cmd);
        }

        public void AddUsers(string color1, string color2, string color3)
        {
            cmd = new SqlCommand("INSERT INTO MatemasjovTblUser (Color1, Color2, Color3, Dato) VALUES(@Color1, @Color2, @Color3, @Dato)");
            cmd.Parameters.AddWithValue("@Color1", color1);
            cmd.Parameters.AddWithValue("@Color2", color2);
            cmd.Parameters.AddWithValue("@Color3", color3);
            cmd.Parameters.AddWithValue("@Dato", DateTime.Now);

            dbAccess.ModifyDataGetScope(cmd);

        }

        public void AddProgress(int userId, string gameName, int progress)
        {
            cmd = new SqlCommand("INSERT INTO MatemasjovTblProgress (UserId, GameName, Progress, Dato) VALUES(@UserId, @GameName, @Progress, @Dato)");
            cmd.Parameters.AddWithValue("@UserId", userId);
            cmd.Parameters.AddWithValue("@GameName", gameName);
            cmd.Parameters.AddWithValue("@Progress", progress);
            cmd.Parameters.AddWithValue("@Dato", DateTime.Now);
            dbAccess.ModifyData(cmd);
        }

    }
}
