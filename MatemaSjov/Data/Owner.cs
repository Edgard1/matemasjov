﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatemaSjov.Data
{
    class Owner
    {
        private DbAccess dbAccess = new DbAccess();
        private SqlCommand CMD = new SqlCommand();

        public void AddProgress(string gameName, int wrong, int right)
        {
            CMD = new SqlCommand("INSERT INTO MatemasjovTblOwner (GameName, False, True, Dato) VALUES(@GameName, @False, @True, @Dato)");
            CMD.Parameters.AddWithValue("@GameName", gameName);
            CMD.Parameters.AddWithValue("@False", wrong);
            CMD.Parameters.AddWithValue("@True", right);
            CMD.Parameters.AddWithValue("@Dato", DateTime.Now);
            dbAccess.ModifyData(CMD);
        }

        public DataTable GetOwnerByGameName(string GameName)
        {
            CMD = new SqlCommand("SELECT * FROM MatemasjovTblOwner WHERE GameName = @GameName");
            CMD.Parameters.AddWithValue("@GameName", GameName);
            return dbAccess.GetData(CMD);
        }

        public void UpdateWrongAndRightByGameName(string gameName, int wrong, int right)
        {
            CMD = new SqlCommand("UPDATE MatemasjovTblOwner SET False = @False, True = @True where GameName = @GameName");
            CMD.Parameters.AddWithValue("@GameName", gameName);
            CMD.Parameters.AddWithValue("@False", wrong);
            CMD.Parameters.AddWithValue("@True", right);
            CMD.Parameters.AddWithValue("@Dato", DateTime.Now);
            dbAccess.ModifyData(CMD);
        }
    }
}
