﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace MatemaSjov
{
    class Menu : GameObject, IUpdateable, IDrawable
    {
        public event EventHandler Click;
        public event EventHandler Hover;
        public Menu(Texture2D texture, SpriteFont font) : base(texture, font)
        {

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var colour = Color.White;
            if (MouseStatemeent.Instance.IsHovering)
                colour = Color.Gray;

            spriteBatch.Draw(Texture, Rectangle, colour);

            // center the text in the button
            if (!string.IsNullOrEmpty(Text))
            {
                var x = (Rectangle.X + (Rectangle.Width / 2)) - (Font.MeasureString(Text).X / 2);
                var y = (Rectangle.Y + (Rectangle.Height / 2)) - (Font.MeasureString(Text).X / 2);
            }
        }

        public void Update(GameTime gameTime)
        {

            // Hover and click function
            MouseStatemeent.Instance.MakeOverOrClick(Rectangle);
        }
    }
}
